import React, { Component } from 'react';

class Ticket extends Component {
  render() {
    let data = this.props.data;
    let title = data.title.replace(/^Billing rec: /, '');
    return (
        <div className="ticket">
          <h3>{title}</h3>
          <p>{data.assigned_user_name}</p>
        </div>
    );
  }
}

class StateColumn extends Component {
  render() {
    let app = this.props.app;
    let tickets = app.TICKETS_BY_STATE[this.props.ticketState].map(t => {
      return <Ticket data={app.TICKETS_BY_ID[t]} key={t} />;
    });
    let classes = ['column'];
    if (tickets.length === 0) {
      classes.push('empty');
    }

    return (
        <div className={classes.join(' ')}>
          <h2>{this.props.ticketState}</h2>
          <div className="ticket-contain">
            {tickets}
          </div>
        </div>
    );
  }
}

export default StateColumn;
