import React, { Component } from 'react';
import './App.css';
import StateColumn from './StateColumn';
import data from './resources/tagged.json';


let APP_STATE = {
  TICKET_STATES:  ['new', 'open', 'review', 'qa', 'resolved'],
  TICKETS_BY_ID: {},
  TICKETS_BY_STATE: {}
}
APP_STATE.TICKET_STATES.forEach(s => APP_STATE.TICKETS_BY_STATE[s] = []);

class App extends Component {
  constructor(props) {
    super(props);
    data.tickets.forEach(ticket => {
      if(ticket.ticket.state !== 'invalid') {
        APP_STATE.TICKETS_BY_ID[ticket.ticket.number] = ticket.ticket;
        APP_STATE.TICKETS_BY_STATE[ticket.ticket.state].push(ticket.ticket.number);
      }
    });
  }

  render() {
    return (
      <div className="App">
        {APP_STATE.TICKET_STATES.map(
          (s, i) => <StateColumn ticketState={s} app={APP_STATE} key={s} />)}
      </div>
    );
  }
}

export default App;
